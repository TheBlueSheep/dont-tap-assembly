IDEAL
MODEL small
STACK 100h
p186
DATASEG
color db 1h
squareNumber db 0
isBlack db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
didRelaseMouse db 1
scoreMessage db 'Points: $'
randomNum db ?
badColor dw 15
goodColor dw 0
numOfSquares dw 4
secondMessage db 10, 'right click to exit$', 10, 13
score dw 0
note dw 8E5h, 7F4h, 715h, 6ADh, 5F2h, 54Ch, 4B7h, 472h
currTime dw 0
timeLeft dw 20


Clock equ es:6Ch
; --------------------------
; Your variables here
; --------------------------
CODESEG


;t1: y, t2: x
proc sampleColor
	push bp
	mov bp,sp
	pusha
	mov bh,0H
	mov cx,[bp+6]
	mov dx,[bp+4]
	mov ah,0Dh
	int 10h ; in al colour
	mov [color],al
	popa
	pop bp
	ret 4
endp SampleColor	


proc PaintPixel
	push bp
	mov bp, sp
	pusha
	mov bh,0h
	mov cx, [bp +6] ;Xpaint
	mov dx, [bp+4] ;Ypaint
	mov ax, [bp+8] ;Colpaint is in low
	mov ah, 0Ch
	int 10h
	popa
	pop bp
	ret 6
endp PaintPixel


; t1 y, t2: x, t3: color, t4: len
proc PaintRow
	push bp
	mov bp, sp
	pusha
	mov cx, [bp+10]
	loopGo:
	push [bp+8];Colpa
	push [bp +6];Xpa
	push [bp+4];Ypa ; are removed at ret PaintPixel
	call PaintPixel
	mov ax,[bp+6]
	inc ax
	mov [bp+6],ax
	loop loopGo
	popa
	pop bp
	ret 8
endp PaintRow

; t1 y, t2: x, t3: color, t4: len, t5: width
proc PaintRectangle
	push bp
	mov bp, sp
	pusha
	mov cx, [bp+12]
	loopRec:
	push [bp +10]; Lenrec
	push [bp+8] ;Colrec
	push [bp +6] ; Xrec
	push [bp+4] ;Yrec ; are removed at ret PaintPixel
	call PaintRow
	mov ax,[bp+4]
	inc ax
	mov [bp+4], ax
	loop loopRec
	popa
	pop bp
	ret 10
endp PaintRectangle




;draws a key by key index
;push key index and then color
proc MakeSquare
	push bp
	mov bp, sp
	pusha
	
	mov cx, [bp+4]; color
	mov ax, [bp+6];index
	
	push 44
	push 44
	
	push cx
	
	
	mov dl, 4
	div dl
	
	mov dh,ah
	mov dl, al
	xor ax,ax
	;dh reminder
	;dl divided
	
	mov al, dh
	mov bx,45
	mul bl
	mov cx,70
	add ax, cx
	push ax
	
	
	xor ax,ax
	mov al,dl
	mul bl
	add ax, 12
	
	push ax
	call PaintRectangle
	
	popa
	pop	 bp
	ret 4
endp MakeSquare



;Draws The Board
proc PrepareBoard
	push bp
	mov bp, sp
	pusha
	
	push 500
	push 500
	
	push 15
	
	push 50
	push 0
	call PaintRectangle
	
	
	
	
	push 185
	push 191
	
	push 1
	
	push 64
	push 10
	
	
	
	call PaintRectangle
	
	mov bx, [bp+4]
	mov cx,16
	mov ax,0
	; t1 y, t2: x, t3: color, t4: len, t5: width
keys:
	push ax
	push bx
	

	call makeSquare
	inc ax
	loop keys
	
	mov cx, 20
	
makeTimeBar:
	push 3
	push cx
	call drawTimeBarSquare
	loop makeTimeBar
	
	
	popa
	pop bp
	ret 2
endp PrepareBoard

;get x and y and retrns square index
proc getSquareNumber
	push bp
	mov bp, sp
	pusha
	mov dx, [bp+4]
	mov cx, [bp+6]
	;added  constants
	mov bx, 45
	sub cx,70
	sub dx, 12
	mov ax, cx
	div bl;al has column
	mov cl,al
	mov ax,dx
	div bl
	
	xor ah,ah
	mov bl,4
	mul bl
	add cl,al
	mov [squareNumber], cl
	
	popa
	pop bp
	ret 4
endp getSquareNumber

;random num stored in randomNum var, number between 0 - 15
proc generateRandom
	push bp
	mov bp, sp
	pusha
	mov ax,40h
	mov es,ax
	mov ax, es:6Ch
	mov ah, [byte cs:bx]
	xor al, ah
	and ax, 00001111b
	mov [randomNum], al
	popa
	pop bp
	ret 0
endp generateRandom

;add a random new black tile that wasnt clicked
proc addRandomBlackTile
	push bp
	mov bp, sp
	pusha
checkNotBlackAlready:
	call generateRandom
	xor bx,bx
	mov bl,[randomNum]
	mov al, [byte ptr isBlack+bx]
	cmp al,1
	je checkNotBlackAlready
squareNotBlack:
	mov [byte ptr isBlack+bx], 1
	push bx
	push [goodColor]
	call makeSquare
	
	
	popa
	pop bp
	ret 0
endp addRandomBlackTile



;play random key
proc playRandomSound
	push bp
	mov bp,sp
	pusha
	
	in al, 61h
	or al, 11b
	out 61h, al
	
	mov al, 0B6h
	out 43h, al
	
	
	call generateRandom
	mov bl, [randomNum]
	xor bh,bh
	;if number between 0-15 is odd we need to even it
	and bx, 0FFFEh;and with all bits except last one
	mov si, offset note
	mov ax, [si+bx]
	out 42h, al
	mov al, ah
	out 42h, al	
	
	popa
	pop bp
	ret 0
endp playRandomSound



;push the number to print
proc printNum
	push bp
	mov bp, sp
	pusha
;the number to print


	xor dx,dx
	mov cl, 0
	mov ax, [bp+4]
	mov cx,0
    mov dx,0
	
	 ; if number is zero
	cmp ax,0
	jne pushDigits
	push 0
	inc cx
    je printDigits  
pushDigits:
   
    cmp ax,0
    je printDigits   
    mov bx,10        
    div bx                  
    push dx              
    inc cx              
    xor dx,dx
	
    jmp pushDigits
	
    printDigits:
    ;check if count 
    ;is greater than zero
    cmp cx,0
    je exitPrint
          
    ;pop the top of stack
    pop dx
          
    ;add 48 so that it represents the ASCII value of digits
    add dx,48
    mov ah,02h
    int 21h
          
    ;decrease the count
    dec cx
    jmp printDigits

exitPrint:

	popa
	pop bp
	ret 2
endp printNum

proc updateTimer
	push bp
	mov bp, sp
	pusha
	
	mov ax, [Clock]
	mov bx, [currTime]
	;checks if times passes
	sub ax, bx
	cmp ax, 20
	jle dontDec
	
	;if time passed, deletes a part of the bar and decreses the time
	push 0
	push [timeLeft]
	call drawTimeBarSquare
	dec [timeLeft]
	mov ax, [Clock]
	mov [currTime], ax

	
dontDec:

	popa
	pop bp
	ret 0
endp updateTimer

; push color then index
;deletes a part of the bar
proc drawTimeBarSquare
	push bp
	mov bp, sp
	pusha
	
	mov bx, [bp+6];color
	mov ax, [bp+4];index
	
	
	push 8
	push 5
	
	push bx
	
	mov dl, 10
	mul dl
	add ax, 50
	push ax
	push 1
	
	

	call PaintRectangle
	
	popa
	pop bp
	ret 4
endp drawTimeBarSquare



start:
	mov ax, @data
	mov ds, ax
; --------------------------
; Your code here
; --------------------------


	; wait for first change in timer
	mov ax, 40h
	mov es, ax
	mov ax, [Clock]
	mov [currTime], ax
firstTick:
	cmp ax, [Clock]
	je firstTick
	
	
	mov ax, 13h
	int 10h ; change to graphic mode
	
	;prepares the board
	push [badColor]
	call PrepareBoard
	
		
	;start mouse
	mov ax, 0h
	int 33h
	;see mouse
	mov ax,1h
	int 33h
	
	mov cx,[numOfSquares]
	;adds random tiles to board
addStart:
	call addRandomBlackTile
	loop addStart
	
	
mainLoop:
	call updateTimer
	;checks if time is up
	cmp [timeLeft], 0 
	jle gameOver
	
	mov al, [didRelaseMouse]
	cmp al, 1
	je allowClick
	
waitForRelease:
;only if mouse relesed allow player to click agian
	
	mov bx,0
	mov ax, 3h;  which button pressed, in bx, 0 non, 1 left 2 right 3 both
	int 33h;mouse interrupts, CX is column (X) DX is row (Y)
		
	;devide cx by 2 and sub 1 from dx
	
	cmp bx, 0
	je releasedMouse
	
	jmp mainLoop
releasedMouse:

	mov [didRelaseMouse], 1
	jmp mainLoop
	
	
	
	
allowClick:

	mov bx,0
	mov ax, 3h
	int 33h
		
	;devide cx by 2 and sub 1 from dx
	
	cmp bx, 1
	;if nothing was clicked goes back to mainloop
	je onClick
	jmp mainLoop
	
	
	
onClick:
	shr cx,1
	sub dx, 1
	push cx
	push dx
	call sampleColor
	xor ax,ax
	mov al,[color]
	;if clicked bad color it is game over
	cmp ax, [badColor]
	je gameOver;bad click (game over)
	cmp ax, [goodColor];clicked on good square
	je changeSquares
	
	mov [didRelaseMouse],0
	jmp mainLoop
	
	
gameOver:
	in al, 61h
	and al, 11111100b
	out 61h, al

	;unsee mouse
	mov ax,2h
	int 33h
	;paints board red
	push 4
	call PrepareBoard
	;see mouse
	mov ax,1h
	int 33h
	
	mov ax,2
	int 10h
	
	
	mov dx, offset scoreMessage
	mov ah, 9h
	int 21h
	
	mov ax, [score]
	push ax
	call printNum
	
	mov dx, offset secondMessage
	mov ah, 9h
	int 21h
endGame:
	;wait for right click

	mov ax, 3h
	int 33h
		
	cmp bx, 2
	je exit
	

	
	jmp endGame
	
	
changeSquares:
	
	mov ax, [score]
	inc ax
	mov [score], ax
	
	push cx
	push dx
	call getSquareNumber
		
	 
	
	xor ah,ah
	mov al, [squareNumber]
	mov bx,ax
	push ax
	push [badColor]
	;unsee mouse
	mov ax,2h
	int 33h
	call makeSquare
	;see mouse
	mov ax,1h
	int 33h
	mov [didRelaseMouse],0
	call addRandomBlackTile
	mov [byte ptr isBlack+bx], 0
	call playRandomSound
	jmp mainLoop

	
	
	
exit:
	in al, 61h
	and al, 11111100b
	out 61h, al

	mov ax,2
	int 10h
	
	mov ax, 4c00h
	int 21h
END start
